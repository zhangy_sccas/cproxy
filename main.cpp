#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <thread>

#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>


void change(int srcSock, int desSock) {
    char buf[1024];
    while (true) {
        ssize_t n = recv(srcSock, buf, sizeof(buf), 0);
        if (n > 0) {
            ssize_t ok = send(desSock, buf, (size_t) n, 0);
            if (ok < 0) {
                break;
            }
        } else {
            break;
        }
    }
}


int main(int argc, char *argv[]) {

    int listenSock;
    int serConn;
    struct sockaddr_in serverAddr, clientAddr;


    if (argc < 2) {
        printf("Please enter ipaddress and port\n");
        exit(-1);
    }

    char *address = argv[0];
    unsigned short port = (unsigned short) strtoul(argv[1], nullptr, 10);


    if ((listenSock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }

    if ((serConn = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(8888);

    memset(&clientAddr, 0, sizeof(clientAddr));
    clientAddr.sin_family = AF_INET;
    clientAddr.sin_port = htons(port);
    if (inet_pton(AF_INET, address, &clientAddr.sin_addr) <= 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }

    if (connect(serConn, (struct sockaddr *) &clientAddr, sizeof(clientAddr)) < 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }


    if (bind(listenSock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }

    if (listen(listenSock, 1) < 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }

    int cliConn = accept(listenSock, (struct sockaddr *) NULL, NULL);
    if (cliConn < 0) {
        printf("error %d:%s\n", errno, strerror(errno));
        exit(errno);
    }

    std::thread t1(change, cliConn, serConn);
    std::thread t2(change, serConn, cliConn);

    t1.join();
    t2.join();
    close(listenSock);
    close(serConn);
    close(cliConn);
    return 0;
}